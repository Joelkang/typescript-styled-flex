import "jest-styled-components";
import * as React from "react";
import * as Renderer from "react-test-renderer";
import Flex from "../src/Flex";

describe("component <Flex />", () => {
  it("should render in DOM without additional props", () => {
    const result = Renderer.create(<Flex>Test</Flex>).toJSON();

    expect(result).toHaveStyleRule("display", "flex");
    expect(result).toMatchSnapshot();
  });

  it("should have style rules to fill 100%", () => {
    const result = Renderer.create(<Flex full>Test</Flex>).toJSON();

    expect(result).toHaveStyleRule("width", "100%");
    expect(result).toHaveStyleRule("height", "100%");
    expect(result).toHaveStyleRule("flex-basis", "100%");
    expect(result).toMatchSnapshot();
  });

  it("should have style rules to align to center in horizontal and vertical align", () => {
    const result = Renderer.create(<Flex center>Test</Flex>).toJSON();

    expect(result).toHaveStyleRule("align-items", "center");
    expect(result).toHaveStyleRule("justify-content", "center");
    expect(result).toMatchSnapshot();
  });

  it("should allow to have many different props together", () => {
    const result = Renderer.create(
      <Flex center full wrap>
        Test
      </Flex>
    ).toJSON();

    expect(result).toHaveStyleRule("width", "100%");
    expect(result).toHaveStyleRule("height", "100%");
    expect(result).toHaveStyleRule("flex-basis", "100%");
    expect(result).toHaveStyleRule("align-items", "center");
    expect(result).toHaveStyleRule("justify-content", "center");
    expect(result).toHaveStyleRule("flex-wrap", "wrap");
    expect(result).toMatchSnapshot();
  });

  const args = [
    ["display", "inline-flex", { inline: true }],
    ["flex-direction", "row", { row: true }],
    ["flex-direction", "row-reverse", { rowReverse: true }],
    ["flex-direction", "column", { column: true }],
    ["flex-direction", "column-reverse", { columnReverse: true }],
    ["flex-wrap", "nowrap", { nowrap: true }],
    ["flex-wrap", "wrap", { wrap: true }],
    ["flex-wrap", "wrap-reverse", { wrapReverse: true }],
    ["justify-content", "flex-start", { justifyStart: true }],
    ["justify-content", "flex-end", { justifyEnd: true }],
    ["justify-content", "center", { justifyCenter: true }],
    ["justify-content", "space-between", { justifyBetween: true }],
    ["justify-content", "space-around", { justifyAround: true }],
    ["align-content", "flex-start", { contentStart: true }],
    ["align-content", "flex-end", { contentEnd: true }],
    ["align-content", "center", { contentCenter: true }],
    ["align-content", "space-between", { contentSpaceBetween: true }],
    ["align-content", "space-around", { contentSpaceAround: true }],
    ["align-content", "stretch", { contentStretch: true }],
    ["align-items", "flex-start", { alignStart: true }],
    ["align-items", "flex-end", { alignEnd: true }],
    ["align-items", "center", { alignCenter: true }],
    ["align-items", "baseline", { alignBaseline: true }],
    ["align-items", "stretch", { alignStretch: true }],
    ["gap", "5px", { gap: "5px" }],
    ["column-gap", "5px", { columnGap: "5px" }],
    ["row-gap", "5px", { rowGap: "5px" }],
  ];

  it.each(args)(
    'should have "%s: %s" style rule',
    (cssKey, cssValue, props) => {
      const result = Renderer.create(<Flex {...props}>Test</Flex>).toJSON();

      expect(result).toHaveStyleRule(cssKey, cssValue);
      expect(result).toMatchSnapshot();
    }
  );
});
