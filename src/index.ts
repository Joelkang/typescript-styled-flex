import Flex, { IFlexProps } from "./Flex";
import FlexItem, { IFlexItemProps } from "./FlexItem";

export default Flex;
export { FlexItem, IFlexItemProps, IFlexProps };
